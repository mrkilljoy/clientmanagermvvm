﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleAppWPF.Models
{
    /// <summary>
    /// Класс-обертка для управления списком клиентов.
    /// </summary>
    class ClientManager : IDisposable
    {
        private ClientsContext _cnt;

        public ClientManager()
        {
            _cnt = new ClientsContext();
        }

        /// <summary>
        /// Добавить нового клиента.
        /// </summary>
        /// <param name="person">Объект рассматриваемого клиента</param>
        /// <returns></returns>
        public bool AddClient(Person person)
        {
            try
            {
                _cnt.Persons.Add(person);
                _cnt.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Удалить выбранного клиента.
        /// </summary>
        /// <param name="person">Объект рассматриваемого клиента</param>
        /// <returns></returns>
        public bool RemoveClient(Person person)
        {
            try
            {
                Person existing_person = _cnt.Persons.FirstOrDefault(p => p.ID == person.ID);
                if (existing_person == null)
                    return false;

                existing_person.IsDeleted = true;
                _cnt.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Обновить данные о клиенте.
        /// </summary>
        /// <param name="person">Объект рассматриваемого клиента</param>
        /// <returns></returns>
        public bool UpdateClient(Person person)
        {
            try
            {
                var existing_person = _cnt.Persons.FirstOrDefault(p => p.ID == person.ID);
                if (existing_person == null)
                    return false;

                existing_person.FirstName = person.FirstName;
                existing_person.LastName = person.LastName;
                existing_person.DateOfBirth = person.DateOfBirth;

                _cnt.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Получить список доступных клиентов.
        /// </summary>
        /// <returns></returns>
        public List<Person> GetAvailableClients()
        {
            return (from c in _cnt.Persons where c.IsDeleted == false select c).ToList();
        }

        public void Dispose()
        {
            ((IDisposable)_cnt).Dispose();
        }
    }
}
