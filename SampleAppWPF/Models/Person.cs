﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SampleAppWPF.Models
{
    /// <summary>
    /// Класс, описывающий клиента (запись в БД).
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Идентификатор клиента.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Дата рождения.
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Видимость записи.
        /// </summary>
        public bool IsDeleted { get; set; } = false;
    }
}
