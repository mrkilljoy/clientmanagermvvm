namespace SampleAppWPF.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    /// <summary>
    /// �������� �� ��� ������ � ���������.
    /// </summary>
    public class ClientsContext : DbContext
    {
        public ClientsContext()
            : base("name=ClientsContext")
        {
            // ������� ��������� ��������� ��
            if (Database.Exists())
                Database.SetInitializer(new NullDatabaseInitializer<ClientsContext>());
        }
        
         public virtual DbSet<Person> Persons { get; set; }
    }
}