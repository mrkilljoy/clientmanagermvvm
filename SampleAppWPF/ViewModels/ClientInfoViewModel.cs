﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using SampleAppWPF.Commands;
using SampleAppWPF.Models;

namespace SampleAppWPF.ViewModels
{
    /// <summary>
    /// Модель представления для добавления/редактирования записей.
    /// </summary>
    class ClientInfoViewModel : ViewModelBase
    {
        private DelegateCommand<Window> _applyRecordCmd;
        private DelegateCommand<Window> _closeWndCmd;
        private Person _currentRecord;

        #region Свойства
        /// <summary>
        /// Рассматриваемая запись.
        /// </summary>
        public Person CurrentRecord
        {
            get { return _currentRecord; }
            set
            {
                _currentRecord = value;
                OnPropertyChanged("CurrentRecord");
            }
        }

        /// <summary>
        /// Статус рассматриваемой записи.
        /// </summary>
        public bool IsNewRecord { get; set; }
        #endregion

        #region Команды
        /// <summary>
        /// Команда на добавление записи.
        /// </summary>
        public ICommand ApplyRecordCommand
        {
            get
            {
                return _applyRecordCmd ?? (_applyRecordCmd = new DelegateCommand<Window>(delegate (object w)
                {
                    bool operation_result = false;

                    // если запись новая - добавить в БД,
                    // иначе - просто внести изменения
                    if (IsNewRecord)
                        operation_result = AddNewClient();
                    else
                        operation_result = UpdateSelectedClient();

                    // если результат добавления положителен - закрыть окно,
                    // иначе - вывести сообщение
                    if (operation_result)
                        CloseWindow(w as Window, operation_result);
                    else
                        MessageBox.Show(messageBoxText: "Something was wrong!", caption: "Oops!");
                }));
            }
        }

        /// <summary>
        /// Команда на закрытие окна.
        /// </summary>
        public ICommand CloseWindowCommand
        {
            get
            {
                return _closeWndCmd ?? (_closeWndCmd = new DelegateCommand<Window>(
                    delegate (object w) { CloseWindow(w as Window, false); })); // w - параметр для выполняемой команды (ссылка на текущее окно)
            }
        }
        #endregion

        #region Методы
        /// <summary>
        /// Добавить новую запись в БД.
        /// </summary>
        /// <returns></returns>
        private bool AddNewClient()
        {
            using (var mng = new ClientManager())
            {
                if (CurrentRecord == null)
                    return false;

                return mng.AddClient(CurrentRecord);
            }
        }

        /// <summary>
        /// Обновить выбранную запись.
        /// </summary>
        /// <returns></returns>
        private bool UpdateSelectedClient()
        {
            using (var mng = new ClientManager())
            {
                if (CurrentRecord == null)
                    return false;

                return mng.UpdateClient(CurrentRecord);
            }
        }

        /// <summary>
        /// Закрыть текущее окно.
        /// </summary>
        /// <param name="window">Объект текущего окна.</param>
        private void CloseWindow(Window window, bool result)
        {
            if (window != null)
            {
                window.DialogResult = result; // результат операции
                window.Close();
            }   
        }
        #endregion
    }
}
