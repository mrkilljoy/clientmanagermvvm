﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Input;
using SampleAppWPF.Commands;
using SampleAppWPF.Models;

namespace SampleAppWPF.ViewModels
{
    /// <summary>
    /// Модель основного представления (MainWindow).
    /// </summary>
    class MainWindowViewModel : ViewModelBase
    {
        private DelegateCommand _addRecordCmd;
        private DelegateCommand _delClientCmd;
        private DelegateCommand _editRecordCmd;

        private ClientManager _mng;
        private ObservableCollection<Person> _clients;
        private Person _selectedRecord;

        public MainWindowViewModel()
        {
            _mng = new ClientManager();
        }

        #region Свойства
        /// <summary>
        /// Список клиентов.
        /// </summary>
        public ObservableCollection<Person> Clients
        {
            get
            {
                _clients = new ObservableCollection<Person>(list: _mng.GetAvailableClients());
                return _clients;
            }
            set
            {
                _clients = value;
                OnPropertyChanged("Clients");
            }
        }

        /// <summary>
        /// Выбранный клиент.
        /// </summary>
        public Person SelectedRecord
        {
            get { return _selectedRecord; }
            set
            {
                _selectedRecord = value;
                OnPropertyChanged("SelectedRecord");

                // изменение доступности клавиш редактирования
                // и удаления записей
                OnPropertyChanged("CanEditClient");
                OnPropertyChanged("CanRemoveClient");
            }
        }

        /// <summary>
        /// Проверить возможность редактирования записи.
        /// </summary>
        public bool CanEditClient
        {
            get { return SelectedRecord != null; }
        }

        /// <summary>
        /// Проверить возможность удалить запись.
        /// </summary>
        public bool CanRemoveClient
        {
            get { return SelectedRecord != null; }
        }
        #endregion

        #region Команды

        /// <summary>
        /// Команда на вызов окна для добавления клиента.
        /// </summary>
        public ICommand AddRecordCommand
        {
            get
            {
                return _addRecordCmd ?? (_addRecordCmd = new DelegateCommand(OpenRecordWindow));
            }
        }

        /// <summary>
        /// Команда на удаление выделенной записи.
        /// </summary>
        public ICommand RemoveClientCommand
        {
            get
            {
                return _delClientCmd ?? (_delClientCmd = new DelegateCommand(RemoveClient));
            }
        }

        /// <summary>
        /// Команда на редактирование выделенной записи.
        /// </summary>
        public ICommand EditClientCommand
        {
            get
            {
                return _editRecordCmd ?? (_editRecordCmd = new DelegateCommand(delegate { EditClient(SelectedRecord); }));
            }
        }
        #endregion

        #region Методы

        /// <summary>
        /// Вызвать окно для добавления клиента.
        /// </summary>
        private void OpenRecordWindow()
        {
            var wnd = new Views.ClientInfoWindow();
            var vmodel = new ViewModels.ClientInfoViewModel()
            {
                IsNewRecord = true,     
                CurrentRecord = new Person() // передаем новый экземпляр для заполнения
            };

            wnd.DataContext = vmodel;
            var result = wnd.ShowDialog();

            // в случае положительного ответа по закрытию формы - 
            // обновляем содержимое коллекции
            if (result != null && result == true)
                OnPropertyChanged("Clients");
        }
        
        /// <summary>
        /// Удалить выбранного клиента.
        /// </summary>
        private void RemoveClient()
        {
            if (SelectedRecord == null)
                return;

            if (_mng.RemoveClient(SelectedRecord))
                OnPropertyChanged("Clients");
        }

        /// <summary>
        /// Вызвать окно для редактирования клиента.
        /// </summary>
        /// <param name="person">Клиент для редактирования.</param>
        private void EditClient(Person person)
        {
            if (person == null)
                return;

            var wnd = new Views.ClientInfoWindow();
            var vmodel = new ViewModels.ClientInfoViewModel
            {
                IsNewRecord = false,
                CurrentRecord = person // передаем экземпляр из коллекции
            };

            wnd.DataContext = vmodel;
            var result = wnd.ShowDialog();

            // обновляем коллекцию при успешном ответе
            if (result != null && result == true)
                OnPropertyChanged("Clients");
        }
        #endregion
    }
}
